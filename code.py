def encrypt(password):
    # 交换个位和十位
    swapped_password = str(password)[1] + str(password)[0] + str(password)[2:]
    
    # 每位数加上5，对10求余，再乘以2
    encrypted_password = ''
    for digit in swapped_password:
        encrypted_password += str((int(digit) + 5) % 10 * 2)
    
    # 反转所有数字
    reversed_password = int(encrypted_password[::-1])
    
    return reversed_password
    # 补充注释
def decrypt(password):
    # 将数字反转
    reversed_password = int(str(password)[::-1])
    
    # 将每位数除以2，然后减去5，加10取余
    decrypted_password = ''
    for digit in str(reversed_password):
        decrypted_password += str((int(digit) // 2 - 5 + 10) % 10)
    
    # 恢复交换个位和十位的顺序
    original_password = int(decrypted_password[1] + decrypted_password[0] + decrypted_password[2:])
    
    return original_password

def main():
    print("==============================")
    print("欢迎使用密码加密解密系统")
    print("==============================")

    while True:
        print("请选择操作：")
        print("1. 加密")
        print("2. 解密")
        print("3. 退出")

        choice = input("请输入选项序号：")
        
        if choice == '1':
            try:
                password = int(input("请输入要加密的数字密码："))
                encrypted_result = encrypt(password)
                print("加密后的结果是：{}".format(encrypted_result))
            except ValueError:
                print("错误：请输入有效的数字密码")
        elif choice == '2':
            try:
                password = int(input("请输入要解密的数字密码："))
                decrypted_result = decrypt(password)
                print("解密后的结果是：{}".format(decrypted_result))
            except ValueError:
                print("错误：请输入有效的数字密码")
        elif choice == '3':
            print("感谢使用密码加密解密系统，再见！")
            break
        else:
            print("错误：请输入有效的菜单选项")

if __name__ == "__main__":
    main()
